import React from 'react';

class List extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          filtered: []
      };
      this.handleChange = this.handleChange.bind(this);
  }
  
  componentDidMount() {
  this.setState({
    filtered: this.props.items
  });
}

componentWillReceiveProps(nextProps) {
  this.setState({
    filtered: nextProps.items
  });
}
  
  handleChange(e) {
  let y = [];
  let newList = [];
      
  if (e.target.value !== "") {
    y = this.props.items;
          
    newList = y.filter(item => {
      const x = item.toLowerCase();
      const filter = e.target.value.toLowerCase();
      return x.includes(filter);
    });
  } else {
    newList = this.props.items;
  }
  this.setState({
    filtered: newList
  });
}
  
  render() {
      return (
          <div>
              <input type="text"
                     className="search" 
                     onChange={this.handleChange}
                     placeholder="Search..." />
                  <ul className="list">
                      {this.state.filtered.map(item => (
                          <li className="lis" key={item}>
                              {item} &nbsp;
                              <button
                                  className="btn"
                                  onClick={() => this.props.delete(item)}
                                  > X </button>
                          </li>
                      ))}
                  </ul>
              </div>
      )
  }
}
  
export default List;


