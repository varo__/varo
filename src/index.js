import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import List from "./App.js"


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: ["Exercice 1"]
    };
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }

  addItem(e) {
    e.preventDefault();

    let list = this.state.list;
    const newItem = document.getElementById("addInput");
    const form = document.getElementById("addItemForm");

    if (newItem.value !== "") {
      list.push(newItem.value);
      this.setState({
        list: list
      });
      newItem.classList.remove("is-danger");
      form.reset();
    } else {
      newItem.classList.add("is-danger");
    }
  }

  removeItem(item) {
    const list = this.state.list.slice();
    list.some((el, i) => {
      if (el === item) {
        list.splice(i, 1);
        return true;
      }else{
        return false;
      }
    });
    this.setState({
      list: list
    });
  }
  
  render() {
    return (
      <div>    
        <h1 className="top">
          To-Do-List
        </h1>
        <input  type="text"
                className="inp"
                id="addInput"
                placeholder="Add..."
              />
              <button className="addbut" onClick={this.addItem}>
                Add Item
              </button>
          <section className="">
                        <List items={this.state.list} delete={this.removeItem} />
          </section>
          
           <section>
            <form className="form" id="addItemForm">
            </form>
          </section> 
      </div>
    );
  }
}



ReactDOM.render(<App />, document.getElementById("root"));

